function [ rms_out, rmspercentage ] = rmsimon( modelx, modely, measurex, measurey )
%RMS easy rms when sequences are equal length and equal indexes,
%difficultier when inequal length, only works when measurement sequence is
%on the shortest domain (or equal), explodes in every other case.
%make sure the model sequence is detailed enough, else interpolating
%becomes less accurate


if length(modelx) ~= length(modely) 
   error('x1 and y1 are not equal in size'); 
end
if length(measurex) ~= length(measurey)
    error('x2 and y2 are not equal in size');
end

if length(modelx) == length(measurex)
    if modelx == measurex
        rms_out = rms(modely-measurey);
        return;
    end
end

%first sort input on x index (could be reversed and stuff, dont want to
%deal with that

[x1, I] = sort(modelx);
y1 = modely(I);
[x2, I] = sort(measurex);
y2 = measurey(I);

% replace the model sequence with a sequence with xes which equal measured xes
for i = 1:length(x2)
   xgoal = x2(i); % target x to match with model values 
   [~, index] = min(abs(x1 - xgoal)); % xtry is closest value to xgoal
   xtry1 = x1(index);
   if xgoal == xtry1
       y3(i) = y1(index); % if they are equal just take the value of the model at that point
   elseif index == 1 && xgoal < xtry1
       error('Model sequence doesnt overlap measured sequence');
   elseif index == length(x1) && xgoal > xtry1
       error('Model sequence doesnt overlap measured sequence');
   else
       %funny part starts here
       if xtry1 < xgoal % x is between xtry and xtry + 1
           xtry2 = x1(index + 1);
           ytry2 = y1(index + 1);
       else % x is between xtry and xtry - 1
           xtry2 = x1(index - 1);
           ytry2 = y1(index - 1);
       end
       
       distance = abs(xtry2 - xtry1);
       try1part = abs(xgoal - xtry1)/distance; 
       try2part = abs(xgoal - xtry2)/distance;
       y3(i) = y1(index) * try2part + ytry2 * try1part; %inverse cuz it is
   end
end


% plot(x1, y1, 'r*'); hold on; plot(x2, y2, 'b*'); plot(x2, y3, 'g*');
% legend('Model', 'Measurement', 'New Model Sequence', 'Location', 'best');

rms_out = rms(y3 - y2);
rmspercentage = rms(100*(y3 - y2)/y2); %rmserror as precentage of measurement